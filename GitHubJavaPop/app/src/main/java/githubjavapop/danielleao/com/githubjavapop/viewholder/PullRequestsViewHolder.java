package githubjavapop.danielleao.com.githubjavapop.viewholder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;

import java.util.ArrayList;

import githubjavapop.danielleao.com.githubjavapop.R;
import githubjavapop.danielleao.com.githubjavapop.entities.PullRequest;
import githubjavapop.danielleao.com.githubjavapop.entities.User;
import githubjavapop.danielleao.com.githubjavapop.service.CircularNetworkImageView;
import githubjavapop.danielleao.com.githubjavapop.service.CustomVolleyRequest;

/**
 * Created by danielleao on 21/12/17.
 */

public class PullRequestsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView pullRequestTitle;
    private TextView pullRequestDescription;
    private CircularNetworkImageView userAvatar;
    private ImageLoader avatarLoader;
    private TextView userUsername;

    private Context context;
    private ArrayList<PullRequest> pullRequests;


    public PullRequestsViewHolder(View itemView, Context context, ArrayList<PullRequest> pullRequests) {
        super(itemView);

        itemView.setOnClickListener(this);

        this.pullRequestTitle       = (TextView) itemView.findViewById(R.id.textPullRequestTitle);
        this.pullRequestDescription = (TextView) itemView.findViewById(R.id.textPullRequestDescription);
        this.userAvatar             = (CircularNetworkImageView) itemView.findViewById(R.id.imgUserAvatar);
        this.userUsername           = (TextView) itemView.findViewById(R.id.textUserUsername);
        this.context                = context;
        this.pullRequests           = pullRequests;
    }

    public void bindData(final PullRequest pullRequest, final User user) {
        this.pullRequestTitle.setText(pullRequest.getTitle());
        this.pullRequestDescription.setText(pullRequest.getBody());
        this.userUsername.setText(user.getLogin());
        this.avatarLoader = CustomVolleyRequest.getInstance(context).getImageLoader();

        String avatarUrl = user.getAvatar_url();
        this.avatarLoader.get(avatarUrl,ImageLoader.getImageListener(this.userAvatar,
                R.mipmap.ic_launcher,
                R.mipmap.ic_launcher));
        this.userAvatar.setImageUrl(avatarUrl, this.avatarLoader);
    }

    @Override
    public void onClick(View v) {
        int position = getAdapterPosition();
        String url = pullRequests.get(position).getHtml_url();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }
}
