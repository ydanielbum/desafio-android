package githubjavapop.danielleao.com.githubjavapop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import githubjavapop.danielleao.com.githubjavapop.R;
import githubjavapop.danielleao.com.githubjavapop.entities.Repository;
import githubjavapop.danielleao.com.githubjavapop.entities.User;
import githubjavapop.danielleao.com.githubjavapop.viewholder.RepositoriesViewHolder;

/**
 * Created by danielleao on 13/12/17.
 */

public class RepositoryListAdapter extends RecyclerView.Adapter<RepositoriesViewHolder> {

    private ArrayList<Repository> repositories;

    public RepositoryListAdapter(ArrayList<Repository> repositories) {
        this.repositories = repositories;
    }


    @Override
    public RepositoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View repositoryView = inflater.inflate(R.layout.row_repository_list, parent,false);

        return new RepositoriesViewHolder(repositoryView,context,repositories);

    }

    @Override
    public void onBindViewHolder(RepositoriesViewHolder holder, int position) {

        Repository repository = repositories.get(position);
        User owner = repository.getOwner();

        holder.bindData(repository,owner);
    }


    @Override
    public int getItemCount() {
        return this.repositories.size();
    }

}
