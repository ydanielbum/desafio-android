package githubjavapop.danielleao.com.githubjavapop.service;

import java.util.List;

import githubjavapop.danielleao.com.githubjavapop.entities.GitHubItems;
import githubjavapop.danielleao.com.githubjavapop.entities.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by danielleao on 13/12/17.
 */

public interface GitHubService {

    public static final String BASE_URL = "https://api.github.com/";

    @GET("search/repositories")
    Call <GitHubItems> getRepositories(
            @Query("q") String q,
            @Query("sort") String sort,
            @Query("page") int page
    );


    @GET("repos/{owner}/{repository}/pulls")
    Call <List<PullRequest>> getPullRequests(
            @Path("owner") String owner,
            @Path("repository") String repository,
            @Query("state") String state
    );

}
