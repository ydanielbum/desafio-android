package githubjavapop.danielleao.com.githubjavapop.entities;

/**
 * Created by danielleao on 14/12/17.
 */

public class User {
    private String login;
    private String avatar_url;


    //Getters and Setters
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }
}
