package githubjavapop.danielleao.com.githubjavapop.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by danielleao on 13/12/17.
 */

public class GitHubItems {

    private ArrayList<Repository> items;

    public ArrayList<Repository> getItems() {
        return items;
    }

    public void setItems(ArrayList<Repository> items) {
        this.items = items;
    }
}
