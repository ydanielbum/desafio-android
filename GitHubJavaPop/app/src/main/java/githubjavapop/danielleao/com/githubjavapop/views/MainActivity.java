package githubjavapop.danielleao.com.githubjavapop.views;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import githubjavapop.danielleao.com.githubjavapop.R;
import githubjavapop.danielleao.com.githubjavapop.adapter.RepositoryListAdapter;
import githubjavapop.danielleao.com.githubjavapop.entities.GitHubItems;
import githubjavapop.danielleao.com.githubjavapop.listener.EndlessRecyclerViewScrollListener;
import githubjavapop.danielleao.com.githubjavapop.service.GitHubService;
import githubjavapop.danielleao.com.githubjavapop.entities.Repository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private RepositoryListAdapter repositoryListAdapter;
    private ArrayList<Repository> repositories = new ArrayList<Repository>();
    private DividerItemDecoration dividerItemDecoration;
    private EndlessRecyclerViewScrollListener endlessScrollListener;
    private int page = 1 ;
    private static final String q = "language:Java";
    private static final String sort = "stars";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerRepositories);


        //Get Repositories
        this.getRepositories(page);


        //Adapter
        repositoryListAdapter = new RepositoryListAdapter(repositories);
        recyclerView.setAdapter(repositoryListAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        //Divider
        dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                linearLayoutManager.getOrientation()
        );
        recyclerView.addItemDecoration(dividerItemDecoration);


        //Endless Scroll
        endlessScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                page++;
                getRepositories(page);
            }
        };

        recyclerView.addOnScrollListener(endlessScrollListener);

    }

    public void getRepositories(int page){


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GitHubService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        final Call<GitHubItems> requestItems = service.getRepositories(q,sort,page);

        requestItems.enqueue(new Callback<GitHubItems>() {

            @Override
            public void onResponse(Call<GitHubItems> call, Response<GitHubItems> response) {
                if (response.isSuccessful()) {

                    GitHubItems items = response.body();

                    ArrayList<Repository> repositoriesTemp = items.getItems();

                    int repositorySize = repositoryListAdapter.getItemCount();

//                    for (Repository r : repositoriesTemp){
//                        Log.d("NOME", r.getName());
//                    }

                    repositories.addAll(repositoriesTemp);

                    repositoryListAdapter.notifyItemRangeInserted(repositorySize, repositories.size() - 1);

                }

            }

            @Override
            public void onFailure(Call<GitHubItems> call, Throwable t) {

            }
        });

    }
}
