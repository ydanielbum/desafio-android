package githubjavapop.danielleao.com.githubjavapop.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import githubjavapop.danielleao.com.githubjavapop.R;
import githubjavapop.danielleao.com.githubjavapop.adapter.PullRequestListAdapter;
import githubjavapop.danielleao.com.githubjavapop.adapter.RepositoryListAdapter;
import githubjavapop.danielleao.com.githubjavapop.entities.PullRequest;
import githubjavapop.danielleao.com.githubjavapop.entities.Repository;
import githubjavapop.danielleao.com.githubjavapop.listener.EndlessRecyclerViewScrollListener;
import githubjavapop.danielleao.com.githubjavapop.service.GitHubService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestsActivity extends AppCompatActivity{

        private String repositoryName;
        private String ownerName;
        private RecyclerView recyclerView;
        private PullRequestListAdapter pullRequestListAdapter;
        private ArrayList<PullRequest> pullRequests = new ArrayList<PullRequest>();
        private DividerItemDecoration dividerItemDecoration;
        private EndlessRecyclerViewScrollListener endlessScrollListener;
        private String state = "all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_requests);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerPullRequests);

        Intent intent = getIntent();
        repositoryName = intent.getStringExtra("repository");
        ownerName = intent.getStringExtra("owner");


        //Log.d("repository",repositoryName);
        //Log.d("owner", ownerName);


        //Get PullRequests
        this.getPullRequests();


        //Adapter
        pullRequestListAdapter = new PullRequestListAdapter(pullRequests);
        recyclerView.setAdapter(pullRequestListAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        //Divider
        dividerItemDecoration = new DividerItemDecoration(
                recyclerView.getContext(),
                linearLayoutManager.getOrientation()
        );
        recyclerView.addItemDecoration(dividerItemDecoration);


    }



    public void getPullRequests(){

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(GitHubService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHubService service = retrofit.create(GitHubService.class);

        final Call<List<PullRequest>> requestPullRequests = service.getPullRequests( ownerName,repositoryName,state);

        requestPullRequests.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if (response.isSuccessful()) {

                    pullRequests = (ArrayList<PullRequest>) response.body();

                    pullRequestListAdapter.setPullRequests(pullRequests);

                    pullRequestListAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {

            }
        });
    }
}
