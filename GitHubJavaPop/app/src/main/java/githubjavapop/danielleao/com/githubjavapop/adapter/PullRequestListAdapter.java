package githubjavapop.danielleao.com.githubjavapop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import githubjavapop.danielleao.com.githubjavapop.R;
import githubjavapop.danielleao.com.githubjavapop.entities.PullRequest;
import githubjavapop.danielleao.com.githubjavapop.entities.Repository;
import githubjavapop.danielleao.com.githubjavapop.entities.User;
import githubjavapop.danielleao.com.githubjavapop.viewholder.PullRequestsViewHolder;
import githubjavapop.danielleao.com.githubjavapop.viewholder.RepositoriesViewHolder;

/**
 * Created by danielleao on 13/12/17.
 */

public class PullRequestListAdapter extends RecyclerView.Adapter<PullRequestsViewHolder> {

    private ArrayList<PullRequest> pullRequests;

    public PullRequestListAdapter(ArrayList<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    @Override
    public PullRequestsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View pullRequestView = inflater.inflate(R.layout.row_pull_request_list, parent,false);

        return new PullRequestsViewHolder(pullRequestView,context, pullRequests);
    }

    @Override
    public void onBindViewHolder(PullRequestsViewHolder holder, int position) {


        PullRequest pullRequest = pullRequests.get(position);

        User user = pullRequest.getUser();

        holder.bindData(pullRequest,user);
    }


    @Override
    public int getItemCount() {

        return this.pullRequests.size();
    }

    public void setPullRequests(ArrayList<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }
}
